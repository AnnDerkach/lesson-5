
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


class Sorting {
	
	public static void main(String[] args){
		try {
			Sort in=null;
			ReadFile file=new ReadFile();
			WriteFile out=new WriteFile();
			switch (SortType.validate(args)){
				case "QUICKSORT":
					in=new QuickSort();
					break;
				case "BUBBLE":
					in=new BubbleSort();
					break;
				case "SHIFT":
					in=new ShiftSort();
					break;
			}	
			out.write(args,in.sortArray(file.read(args)));
		} 
		catch (Exception e) {
			System.err.println(e); 
			System.exit(1);
		}
	}
}


enum SortType {
	BUBBLE, QUICKSORT, SHIFT;
	
	public static String validate(String[] args) {
		switch (args[0]) {
		case "bubble":
			return SortType.BUBBLE.toString();
		case "qsort":
			return SortType.QUICKSORT.toString();
		case "shift":
			return SortType.SHIFT.toString();
		default:
			System.err.println("There is no such sort method");
			System.exit(1);
		}
		return null;
		
	}
}


enum FileTypeRead {
	TEXT, STDIN, CSV, XML;
	public static String defineType(String[] args){
		try{
			switch (args[1]) {
			case "txt":
				return FileTypeRead.TEXT.toString();
			case "stdin":
				return FileTypeRead.STDIN.toString();
			case "csv":
				return FileTypeRead.CSV.toString();
			case "xml":
				return FileTypeRead.XML.toString();
			default:
				System.err.println("There is no such file type to read");
				System.exit(1);
			}
		}
        catch(Exception e){
        	e.printStackTrace();
		}
		return null;
	}		
}


class ReadFile{
	public String fileName;
	List<Integer> array=new ArrayList<Integer>();
	
	public List<Integer> read(String[] args){
		try{
			fileName=args[2];
			switch (FileTypeRead.defineType(args)){
				case "TEXT":
				    Scanner in=new Scanner(new File(fileName));
				    while (in.hasNextLine())
				    	array.add(Integer.parseInt(in.nextLine()));
					break;
				case "STDIN":
					System.out.println("Initialize the array");
					Scanner sc=new Scanner(System.in);
					String s=sc.nextLine();
					for (String prop: s.split(" "))
						array.add(Integer.parseInt(prop));
					break;
				case "CSV":
					BufferedReader buff=new BufferedReader(new FileReader(fileName));
					String line=buff.readLine();
					for (String prop: line.split(","))
						array.add(Integer.parseInt(prop));
					break;
				case "XML":
					File fXml=new File(fileName);
					DocumentBuilderFactory dbf=DocumentBuilderFactory.newInstance();
		            DocumentBuilder db=dbf.newDocumentBuilder();
		            Document doc=db.parse(fXml);      
		            doc.getDocumentElement().normalize();	  
		            NodeList nodeLst=doc.getElementsByTagName("value");
		            for(int i=0;i<nodeLst.getLength();i++){
		                Node fstNode=nodeLst.item(i);
		                if(fstNode.getNodeType()==Node.ELEMENT_NODE){
		                	 Element elj=(Element)fstNode;
		                     String elem=elj.getTextContent();
		                     array.add(Integer.parseInt(elem));
		                }
		            }	
		            break;
			}
		}
		catch(Exception e){
			e.printStackTrace();
			System.exit(1);
		}
		return array;
	}
}

enum  FileTypeWrite{
	TEXT, STDOUT, CSV, XML;
	public static String defineWriteType(String[] args){
		try{
			switch (args[3]) {
			case "txt":
				return FileTypeWrite.TEXT.toString();
			case "stdout":
				return FileTypeWrite.STDOUT.toString();
			case "csv":
				return FileTypeWrite.CSV.toString();
			case "xml":
				return FileTypeWrite.XML.toString();
			default:
				System.err.println("There is no such file type to write");
				System.exit(1);
			}
		}
        catch(Exception e){
        	e.printStackTrace();
		}
		return null;
	}		
}

class WriteFile{
	public String fileName;
	
	public void write(String[] args,List<Integer> array){
		try{
			fileName=args[4];
			switch (FileTypeWrite.defineWriteType(args)){
				case "TEXT":
					File file = new File(fileName);
					if(!file.exists())
				            file.createNewFile();
					PrintWriter out=new PrintWriter(file.getAbsoluteFile());
					try{
						 out.print(array);
					}
					finally {
			            out.close();
			        }
					break;
				case "STDOUT":
					for(int i=0;i<array.size();i++)
						System.out.print(array.get(i)+" ");
					break;
				case "CSV":
					PrintWriter writer = new PrintWriter(fileName);
					try{
						for(int i=0;i<array.size();i++) 
							writer.append(array.get(i)+" ");
						writer.flush();
					}
					finally {
						writer.close();
					}
					break;
				case "XML":
					File fXml=new File(fileName);
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		            DocumentBuilder builder=factory.newDocumentBuilder();
		            Document doc=builder.newDocument();
		            Element RootElement=doc.createElement("root");  
		            Element RootElement1=doc.createElement("values");  
		            RootElement.appendChild(RootElement1);
		            for(int i=0;i<array.size();i++){
		            	Element NameElementTitle=doc.createElement("value");
		            	NameElementTitle.appendChild(doc.createTextNode(array.get(i).toString()));
		                RootElement1.appendChild(NameElementTitle);
		            }
		            doc.appendChild(RootElement);
		            Transformer t=TransformerFactory.newInstance().newTransformer();
	                t.transform(new DOMSource(doc), new StreamResult(new FileOutputStream("out.xml")));
		            break;
			}
		}
		catch(Exception e){
			e.printStackTrace();
			System.exit(1);
		}
	}
}
	

interface Sort{
	public List<Integer> sortArray(List<Integer> array) throws Exception;
}

class QuickSort implements Sort {
	private List<Integer> array;

	public List<Integer> sortArray(List<Integer> array) throws Exception {
		this.array=array;
	    if (this.array==null||this.array.size()==0)
	    	throw new Exception("Set the value in the file");
	    int size=this.array.size();
	    System.out.println("Before Quick sorting: "+array);
	    System.out.println("After Quick sorting: "+qSort(0, size-1));
	    return this.array;
	}
	
	public List<Integer> qSort(int start, int end){
		try{
			if (start>=end)
	            return array;
	        int i=start,j=end;
	        int cur=i-(i-j)/2;
		    while (i<j){
	            while(i<cur&&(array.get(i)<=array.get(cur))){
	            	i++;
	            }
	            while(j>cur&&(array.get(cur)<=array.get(j))) {
	                j--;
	            }
	            if (i<j) {
	                int temp=array.get(i);
	                array.set(i, array.get(j));
	                array.set(j, temp);
	                if (i==cur)
	                   cur=j;
	                else if (j==cur)
	                   cur=i;
	            }
	        }
	        qSort(start, cur);
	        qSort(cur+1, end);
		}
		catch(Exception e){
			System.err.println(e); 
			System.exit(1);
		}
		return array;
	}

}

class BubbleSort implements Sort{
	private List<Integer> array;

	public List<Integer> sortArray(List<Integer> array) throws Exception {
		this.array=array;
	    if (this.array==null||this.array.size()==0)
	    	throw new Exception("Set the value in the file");
	    System.out.println("Before Bubble sorting: "+array);
		for(int i=0;i<this.array.size()-1;i++)
			for(int j=i+1;j<this.array.size();j++)
				if(this.array.get(i)>this.array.get(j)){
					int temp=this.array.get(i);
					this.array.set(i,this.array.get(j));
					this.array.set(j,temp);
				}			
	    System.out.println("After Bubble sorting: "+array);
	    return array;
	}	
}

class ShiftSort implements Sort{
	private List<Integer> array;

	public List<Integer> sortArray(List<Integer> array) throws Exception {
		this.array=array;
	    if (this.array==null||this.array.size()==0)
	    	throw new Exception("Set the value in the file");
		System.out.println("Before Shift sorting: "+array);
	    for(int i=1;i<this.array.size();i++){
	        for(int j=i; j>0 && this.array.get(j-1)>this.array.get(j);j--){
	            int temp=this.array.get(j-1);
	            this.array.set(j-1,this.array.get(j));
	            this.array.set(j,temp);
	        }
	    }
	    System.out.println("After Shift sorting: "+this.array);
	    return this.array;
	}	
}







